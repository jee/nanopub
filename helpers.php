<?php

/**
 * nanopub - MicroPub support for Static Blog Engine
 *
 * PHP version 7
 *
 * @author      Daniel Goldsmith <dgold@ascraeus.org>
 * @copyright   © 2017-2019 Daniel Goldsmith <dgold@ascraeus.org>
 * @license     BSD 3-Clause Clear Licence
 * @link        https://github.com/dg01d/nanopub
 * @category    Micropub
 * @version     2.0.0
 *
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 *
 */


use GuzzleHttp\Client;
use Forecast\Forecast;

/**
 * Uses Compass and DarkSky to obtain weather values
 *
 * @return (array) $weather Weather data from resource
 */

function getWeather()
{
    $configs = include 'configs.php';
    $client = new Client(
        [
            'base_uri' => $configs->compass,
            'query' => [
                    'token' => $configs->compassKey,
                    'geocode' => true
            ]
        ]
    );

    $response = $client->request('GET', 'last');
    $body = json_decode($response->getBody(), true);
    $lat = $body['geocode']['latitude'] ?? $configs->defaultLat;
    $long = $body['geocode']['longitude'] ?? $configs->defaultLong;
    $loc = $body['geocode']['best_name'] ?? $configs->defaultLoc;

    $forecast = new Forecast($configs->forecastKey);
    $weather = $forecast->get(
        $lat,
        $long,
        null,
        array(
            'units' => 'si',
            'exclude' => 'minutely,hourly,daily,alert,flags'
        )
    );

    $response = [];
    $response['loc'] = $loc;
    $response['weather'] = $weather->currently->summary;
    $response['wicon'] = $weather->currently->icon;
    $response['temp'] = (string) round($weather->currently->temperature, 1);
    return $response;
}

/**
 * @since 1.4
 * Tries to obtain metadata from a given url
 *
 * @param $url    The uri of the resource to be parsed
 *
 * @return $resp Array of parsed data from resource
 */


function tagRead($url) 
{
    date_default_timezone_set($configs->timezone);
    $cdate = date('c', time());

    $tags = array();
    $site_html=  file_get_contents($url);
    $site_html = str_replace('data-react-helmet="true"', '', $site_html);
    preg_match_all('/<head>(.*?)<\/head>/si', $site_html, $head);
    $data = $head['0']['0'];

    if ($data !== false ) {
        preg_match_all(
            '/<[\s]*meta[\s]*(name|property)="?' . '([^>"]*)"?[\s]*'
                     . 'content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', 
            $data, $match
        );

        $count = count($match['3']);
        if ($count != 0) {
            $i = 0; do {

                $key = $match['2']["$i"];
                $key = trim($key);
                $value = $match['3']["$i"];
                $value = trim($value);

                $tags["$key"] = "$value"; $i++;
            } while ($i < $count);
        }

    }

    $resp['xAuthor'] = $tags['author'] ?? $tags['article:author'] ?? 
                    $tags['parsely-author'] ?? $tags['twitter:creator'] ?? 
                    $tags['og:site_name'];
    
    $resp['xImage'] = $tags['twitter:image'] ?? $tags['og:image'];
    
    $resp['xContent'] = $tags['title'] ?? $tags['og:title'] ??
                    $tags['twitter:title'] ?? $tags['parsely-title'] ??
		    $tags['sailthru.title'];

    $resp['title'] = $tags['title'] ?? $tags['og:title'] ?? 
                    $tags['twitter:title'] ?? $tags['parsely-title'] ?? 
                    $tags['sailthru.title'];

    $resp['xSummary'] = $tags['twitter:description'] ?? $tags['og:description'] ?? 
                    $tags['description'] ?? 
                    $tags['sailthru.description'];

    $strDate = $tags['article:published_time'] ?? $tags['datePublished'] ?? 
            $tags['date'] ?? $tags['pubdate'] ?? $tags['sailthru.date'] ?? 
            $tags['parsely-pub-date'] ?? $tags['DC.date.issued'] ?? $cdate;

    $resp['xPublished'] = date("c", strtotime($strDate));
    $ymd_date = date("Ymd", $resp['xPublished']);
    if ($ymd_date == "19700101") {
      $resp['xPublished'] = substr($strDate, 0, 19);
    }

    $resp['site'] = $tags['og:site_name'] ?? $tags['twitter:site'];
    return $resp;
}
/**
 * Get Graph from Facebook
 */

function graph_fb($url, $site)
{
  $fb_token = $configs->FB_Token;;
  $fb_graph_url = "https://graph.facebook.com/v4.0/?scrape=true&id="."$url"."&access_token=".$fb_token;

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $fb_graph_url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fb_graph_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 444);
  $data = json_decode(curl_exec($ch), true);
  $info = curl_getinfo($ch);
  curl_close($ch);
  if (empty($data['author'])) {
    $data['xAuthor'] = $site;
  } else {
  $data['xAuthor'] = $data['author'];
  }
  $data['xSummary'] = $data['description'];
  $data['xPublished'] = $data['updated_time'];

  return $data;
}
/**
 * Get Favicon
 */

function favicon_url_grabber($site)
{
  $favicongrabber_url = "http://favicongrabber.com/api/grab/".$site."?pretty=true";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $favicongrabber_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_USERAGENT, 'FaviconBot/1.0 (+http://'.$_SERVER['SERVER_NAME'].'/');
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 444);
  $data = json_decode(curl_exec($ch), true);
  $favicon = $data['icons']['0']['src'];
  $info = curl_getinfo($ch);
  curl_close($ch);
  return $favicon;
}

/**
 * @since 1.2
 * Uses the XRay library to extract rich content from uris
 *
 * @param $url    The uri of the resource to be parsed
 * @param $site   The hostname of the resource to be parsed
 *                Could specify other services in configs.php
 * @return $url_parse Array of parsed data from resource
 */

function xray_machine($url, $site)
{
    $xray = new p3k\XRay();
    if ($site == "twitter.com") {
        $configs = include 'configs.php';
        $twAPIkey = $configs->twAPIkey;
        $twAPIsecret = $configs->twAPIsecret;
        $twUserKey = $configs->twUserKey;
        $twUserSecret = $configs->twUserSecret;
        $url_parse = $xray->parse(
            $url, 
            [
              'timeout' => 30,
              'twitter_api_key' => $twAPIkey,
              'twitter_api_secret' => $twAPIsecret,
              'twitter_access_token' => $twUserKey,
              'twitter_access_token_secret' => $twUserSecret
            ]
        );
    } else {
        $url_parse = $xray->parse($url);
	/*
	print_r(PHP_EOL);
	print_r('DEBUG URL_PARSE');
	print_r($url_parse);
	print_r(PHP_EOL);
	print_r('DEBUG URL_PARSE');
	 */
    }

    $from_tag = tagRead($url);
    /*
    print_r($from_tag);
    print_r(PHP_EOL);
    print_r('DEBUG FROM_TAG');
    print_r($from_tag);
    print_r(PHP_EOL);
    print_r('DEBUG FROM_TAG');
    */
    $from_fb = graph_fb($url, $site);
    /*
    print_r(PHP_EOL);
    print_r('DEBUG FROM_FB');
    print_r($from_fb);
    print_r(PHP_EOL);
    print_r('DEBUG FROM_FB');
    */

    if (empty($url_parse['data']['author']['name'])) {
        if (empty($from_tag['xAuthor'])) {
          $result['xAuthor'] = $from_fb['xAuthor'] ??
          $site;
	} else {
	  $result['xAuthor'] = $from_tag['xAuthor'];
        }
    } else {
        $result['xAuthor'] = $url_parse['data']['author']['name'];
    }
    /*
    if (empty($url_parse['data']['author']['url'])) {
      if (empty($from_tag['xAuthorUrl'])) {
          $result['xAuthorUrl'] = $from_fb['url'] ??
    	$site;
      }
    } else {
      $result['xAuthorUrl'] = $url_parse['data']['author']['url'];
    }
     */
    $result['xAuthorUrl'] = $site;
    
    if (empty($url_parse['data']['name'])) {
      if (empty($url_parse['title'])) {
	    $result['title'] = $from_fb['title'] ??
	    $from_fb['description'] ??
	    substr($url_parse['data']['content']['text'], 0, 42);
      }
    } else {
      $result['title'] = $url_parse['data']['name'];
    }
    
    if (empty($url_parse['data']['content']['text'])) {
	$result['xContent'] = $from_fb['xContent'] ??
	  $from_fb['description'] ?? 
    	  auto_link($xContent, false) ?? 'A Post';
    } else {
	$result['xContent'] = substr($url_parse['data']['content']['text'], 0, 240);
    }
    
    if (empty($url_parse['data']['photo'])) {
        $result['xImage'] = $from_tag['xImage'] ??
          $from_fb['image'];
    } else {
	$result['xImage'] = $url_parse['data']['photo'];
    }

    if (empty($url_parse['data']['author']['photo'])) {
	$result['xPhoto'] = $from_tag['image'] ??
	favicon_url_grabber($site);
    } else {
        $result['xPhoto'] = $url_parse['data']['author']['photo'];
    }
    if (empty($url_parse['data']['summary'])) {
      $result['xSummary'] = $from_tag['xSummary'] ??
	$from_fb['description'] ??
    	$from_fb['title'] ??
    	$from_fb['content'] ??
    	auto_link($xContent, false) ?? 'A Post';
    } else {
        $result['xSummary'] = $url_parse['data']['summary'];
    }
    if (empty($url_parse['data']['published'])) {
      $result['xPublished'] = $from_tag['xPublished'] ??
	$from_fb['xPublished'];
    } else {
      $result['xPublished'] = $url_parse['data']['published'];
    }
      
    /*if (empty($from_tag['site'])) {
        $result['site'] = $from_fb['site'] ??
        $site;
    }*/
    $result['site'] = $site;
    
    if (isset($url_parse['data']['category'])) {
      $result['tags'] = $url_parse['data']['category'];
    }

    return $result;
    /* print_r($result);*/
}
